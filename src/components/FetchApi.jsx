import { Component } from "react";

const vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders"
class FetchApi extends Component {
    fetchApi = async (url, body) => {
        const reqponse = await fetch(url, body);
        const data = reqponse.json()

        return data
    }

    onBtnGetAllOrder = () => {
        this.fetchApi(vBASE_URL)
            .then(data => {
                console.log(data)
            })
            .catch(error => {
                console.log(error);
            })
    }

    onBtnCreateOrder = () => {
        let body = {
            method: 'POST',
            body: JSON.stringify({
                kichCo: "M",
                duongKinh: "25",
                suon: "4",
                salad: "300",
                loaiPizza: "HAWAII",
                idVourcher: "16512",
                idLoaiNuocUong: "PEPSI",
                soLuongNuoc: "3",
                hoTen: "Phạm Thanh Bình",
                thanhTien: "200000",
                email: "binhpt001@devcamp.edu.vn",
                soDienThoai: "0865241654",
                diaChi: "Hà Nội",
                loiNhan: "Pizza đế dày"
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }
        this.fetchApi(vBASE_URL, body)
            .then(data => {
                console.log(data)
            })
            .catch(error => {
                console.log(error);
            })
    }

    onBtnGetOrderByOrderCode = () => {
        this.fetchApi(vBASE_URL + "/f5xR1qpPr3")
            .then(data => {
                console.log(data)
            })
            .catch(error => {
                console.log(error);
            })
    }
    onBtnUpdateOrder = () => {
        let body = {
            method: 'PUT',
            body: JSON.stringify({
                kichCo: "M",
                duongKinh: "25",
                suon: "4",
                salad: "300",
                loaiPizza: "HAWAII",
                idVourcher: "16512",
                idLoaiNuocUong: "PEPSI",
                soLuongNuoc: "3",
                hoTen: "Phạm Thanh Bình",
                thanhTien: "200000",
                email: "binhpt001@devcamp.edu.vn",
                soDienThoai: "0865241654",
                diaChi: "Hà Nội",
                loiNhan: "Pizza đế dày"
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }
        this.fetchApi(vBASE_URL + "/185040", body)
        .then(data => {
            console.log(data)
        })
        .catch(error => {
            console.log(error);
        })
    }

    onBtnCheckVoucherId = () => {
        this.fetchApi("http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/12332")
        .then(data => {
            console.log(data)
        })
        .catch(error => {
            console.log(error);
        })
    }

    onBtnGetDrinkList = () => {
        this.fetchApi("http://203.171.20.210:8080/devcamp-pizza365/drinks")
        .then(data => {
            console.log(data)
        })
        .catch(error => {
            console.log(error);
        })
    }


    render() {
        return (
            <>
                <div className="row">
                    <h3>Fetch API</h3>
                </div>
                <div className="form-group ">
                    <button onClick={this.onBtnGetAllOrder} className="btn btn-primary mx-2">Call api get all orders</button>
                    <button onClick={this.onBtnCreateOrder} className="btn btn-info  mx-2">Call api create order</button>
                    <button onClick={this.onBtnGetOrderByOrderCode} className="btn btn-success  mx-2">Call api get order by orderCode</button>
                    <button onClick={this.onBtnUpdateOrder} className="btn btn-secondary mx-2">Call api update order by id</button>
                    <button onClick={this.onBtnCheckVoucherId} className="btn btn-danger mx-2">Call api check voucher by id</button>
                    <button onClick={this.onBtnGetDrinkList} className="btn btn-success mx-2">Call api Get drink list</button>
                </div>

            </>
        )
    }
}

export default FetchApi