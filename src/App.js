import './App.css';
import "bootstrap/dist/css/bootstrap.min.css"
import FetchApi from './components/FetchApi';
import Axios from './components/AxiosApi';

function App() {
  return (
    <div className="m-5">
      
      <FetchApi/>
      <br></br>
      
      <Axios/>
    </div>
  );
}

export default App;
